const createEventEmitter = require("./event-emitter")
const { sleep } = require("./utils")
const getKeyDescription = require("./keys")

const createEngine = async rpc => {
	const session = await initTarget(rpc)
	const eventEmitter = createEventEmitter()
	await initBrowserEvents(session, rpc, eventEmitter)

	return {
		on: eventEmitter.on,
		unregister: eventEmitter.unregister,
		waitForEvent: eventEmitter.waitForEvent,
		cdpSession: session,
		execute: evt => execute(evt, session, rpc),
		close: () => close(session, rpc),
	}
}

const initTarget = async rpc => {
	let { targetInfo } = await rpc.send({
		method: "Target.getTargetInfo",
		params: {},
	})
	let targetId = targetInfo.targetId

	const isPageTarget = targetInfo.type === "page"
	let browserContextId = null

	if(!isPageTarget) {
		browserContextId = (await rpc.send({
			method: "Target.createBrowserContext",
			params: {},
		})).browserContextId

		targetId = (await rpc.send({
			method: "Target.createTarget",
			params: {
				url: "about:blank",
				browserContextId,
			},
		})).targetId
	}

	const { sessionId } = await rpc.send({
		method: "Target.attachToTarget",
		params: {
			targetId,
			flatten: true,
		},
	})

	await rpc.send({
		method: "Target.activateTarget",
		params: {
			targetId,
		},
	})


	const send = async (msg, expectResponse) =>
		await rpc.send({
			sessionId,
			...msg,
		}, expectResponse)

	await send({
		method: "Page.bringToFront",
		params: {},
	}, false)

	await send({
		method: "Page.enable",
		params: {},
	}, false)


	/**
	 * Here we try to keep track of the top-level executionContextId.
	 * 
	 * Nested contexts (such as in frames) don't interest us at the moment.
	 * 
	 * I actually have no idea if this won't case problems in the future
	 * but it works for now.
	 */
	let executionContextId = null
	rpc.notifications.on("Runtime.executionContextCreated", msg => {
		if(executionContextId === null)
			executionContextId = msg.params.context.id
	})
	rpc.notifications.on("Runtime.executionContextDestroyed", msg => {
		const contextId = msg.params.executionContextId
		if(executionContextId === contextId)
			executionContextId = null
	})
	await send({
		method: "Runtime.enable",
		params: {},
	})
	const getExecutionContextId = () => executionContextId


	const getMainFrame = async () => {
		const { frameTree } = await send({
			method: "Page.getFrameTree",
			params: {},
		})
		return frameTree.frame
	}

	const setLifecycleEventsEnabled = enabled =>
		send({
			method: "Page.setLifecycleEventsEnabled",
			params: {
				enabled,
			},
		})

	const awaitPageLoad = async (loadPromise, frameId = null) => {
		if(frameId === null) frameId = (await getMainFrame()).id
		await setLifecycleEventsEnabled(true)

		const waitForLifecycleEvent = evtName =>
			rpc.notifications.waitForEvent("Page.lifecycleEvent", msg => (
				msg.params.frameId === frameId
				&& msg.params.name === evtName
			))

		/**
		 * We await the following on Page.navigate:
		 *
		 * - the actual navigate request itself
		 *   This should be awaited here in order to avoid the
		 *   possibility that any lifecycle event fires before
		 *   the listeners are registered
		 *
		 * - DOMContentLoaded
		 *
		 * - networkAlmostIdle
		 *   maybe some sites keep connections open
		 *
		 * - firstMeaningfulPaint
		 *   the last event we can measure (I think)
		 */
		await Promise.all([
			loadPromise,
			waitForLifecycleEvent("DOMContentLoaded"),
			waitForLifecycleEvent("networkAlmostIdle"),
			waitForLifecycleEvent("firstMeaningfulPaint"),
		])

		await setLifecycleEventsEnabled(false)
	}


	const inputState = {
		mouse: {
			x: 0,
			y: 0,
			buttons: 0,
		},
	}


	await send({
		method: "Page.setDeviceMetricsOverride",
		params: {
			width: 800,
			height: 600,
			mobile: false,
			deviceScaleFactor: 1.0,
		},
	})


	return {
		targetId,
		browserContextId,
		isPageTarget,
		sessionId,
		inputState,
		send,
		getMainFrame,
		getExecutionContextId,
		awaitPageLoad,
	}
}


const initBrowserEvents = async (session, rpc, eventEmitter) => {
	rpc.notifications.on("Page.frameNavigated", async msg => {
		if(msg.sessionId === session.sessionId) {
			const { url, id: frameId } = msg.params.frame
			const { id: mainFrameId } = await session.getMainFrame()
			if(frameId === mainFrameId)
				eventEmitter.fireEvent("navigated", {
					url,
				})
		}
	})
}


const NONEXISTENT_OP_ERR = "nonexistentOperation"
const NONEXISTENT_EXTRACTION_PROP_ERR = "nonexistentExtractionProperty"
const UNEXPECTED_EXECUTION_RESULT_ERR = "unexpectedExecutionResult"

const execute = async ({ op, args, delayBefore }, session, rpc) => {
	await sleep(delayBefore)

	const _goto = async () => {
		const { id: frameId } = await session.getMainFrame()

		await session.awaitPageLoad(session.send({
			method: "Page.navigate",
			params: {
				url: args.url,
				frameId,
			},
		}), frameId)
	}

	const _reload = async () => {
		await session.awaitPageLoad(session.send({
			method: "Page.reload",
			params: {
				ignoreCache: true,
			},
		}))
	}

	const _navigate = async distance => {
		const { currentIndex, entries } = await session.send({
			method: "Page.getNavigationHistory",
			params: {},
		})

		const newIndex = currentIndex + distance
		if(newIndex < 0 || newIndex > entries.length - 1)
			return

		const navTargetID = entries[newIndex].id
		await session.awaitPageLoad(session.send({
			method: "Page.navigateToHistoryEntry",
			params: {
				entryId: navTargetID,
			},
		}))
	}

	const modifiers = () => {
		let modBits = 0
		if(args.altDown) modBits |= 1
		if(args.ctrlDown) modBits |= 2
		if(args.metaDown) modBits |= 4
		if(args.shiftDown) modBits |= 8
		return modBits
	}

	const mouseButtons = () => {
		let buttonBits = 0
		if(args.leftMouseButtonDown) buttonBits |= 1
		if(args.rightMouseButtonDown) buttonBits |= 2
		if(args.middleMouseButtonDown) buttonBits |= 4
		if(args.backMouseButtonDown) buttonBits |= 8
		if(args.forwardMouseButtonDown) buttonBits |= 16
		return buttonBits
	}

	const _mouseMove = async () => {
		session.inputState.mouse = {
			x: args.x,
			y: args.y,
		}

		await session.send({
			method: "Input.dispatchMouseEvent",
			params: {
				type: "mouseMoved",
				x: args.x + .0,
				y: args.y + .0,
				modifiers: modifiers(),
				buttons: mouseButtons(),
			},
		})
	}

	const _mouseButtonEvent = async evtName => {
		await session.send({
			method: "Input.dispatchMouseEvent",
			params: {
				type: evtName,
				x: session.inputState.mouse.x,
				y: session.inputState.mouse.y,
				button: args.button,
				modifiers: modifiers(),
				clickCount: args.clickCount != null ? args.clickCount : 1,
			},
		})
	}

	const _scroll = async () => {
		await session.send({
			method: "Input.dispatchMouseEvent",
			params: {
				type: "mouseWheel",
				x: session.inputState.mouse.x,
				y: session.inputState.mouse.y,
				deltaX: args.deltaX + .0,
				deltaY: args.deltaY + .0,
				modifiers: modifiers(),
				buttons: mouseButtons(),
			},
		})
	}

	const _keyEvent = async (type, key = args.code) => {
		const keyDesc = getKeyDescription(key, modifiers())
		await session.send({
			method: "Input.dispatchKeyEvent",
			params: {
				type,
				modifiers: modifiers(),
				key: keyDesc.key,
				text: keyDesc.text,
				unmodifedText: keyDesc.text,
				code: keyDesc.code,
				// windowsVirtualKeyCode: keyDef.code,
				location: keyDesc.location,
				autoRepeat: false,
				isKeypad: args.isKeypad,
			},
		})
	}

	const _type = async () => {
		const chars = args.text.split("")
		for(const char of chars) {
			await _keyEvent("keyDown", char)
			await sleep(Math.floor(Math.random() * 75 + 25))
			await _keyEvent("keyUp", char)
		}
	}

	const select = async (selector, baseElemObjectId = undefined) => {
		const { exceptionDetails, result } = await session.send({
			method: "Runtime.callFunctionOn",
			params: {
				functionDeclaration: `(selector, baseElem) =>
					(baseElem ? baseElem : document).querySelector(selector)`,
				executionContextId: await session.getExecutionContextId(),
				arguments: [
					{
						value: selector,
					},
					{
						objectId: baseElemObjectId,
					},
				],
			},
		})
		if(exceptionDetails) throw Error(`${UNEXPECTED_EXECUTION_RESULT_ERR}: select threw an exception: ${JSON.stringify(exceptionDetails)}`)
		if(result.type !== "object") throw Error(`${UNEXPECTED_EXECUTION_RESULT_ERR}: select did not return object; Result: ${JSON.stringify(result)}`)
		return result
	}

	const _focus = async () => {
		const result = await select(args.selector)
		await session.send({
			method: "Runtime.callFunctionOn",
			params: {
				functionDeclaration: "elem => elem.focus()",
				executionContextId: await session.getExecutionContextId(),
				arguments: [
					{
						objectId: result.objectId,
					},
				],
			},
		})
	}

	const _extract = async () => {
		let extractFn = null
		switch(args.prop) {
			case "clientWidth":
			case "offsetWidth":
			case "scrollWidth":
			case "clientHeight":
			case "offsetHeight":
			case "scrollHeight":
			case "innerHTML":
			case "outerHTML":
			case "tagName":
				extractFn = `e.${args.prop}`
				break
			case "text":
				extractFn = "e.innerText"
				break
			case "href":
			case "target":
			case "src":
			case "role":
			case "type":
			case "label":
			case "colspan":
			case "rowspan":
			case "class":
			case "id":
				extractFn = `e.getAttribute("${args.prop}")`
				break
			case "color":
			case "backgroundColor":
				extractFn = `window.getComputedStyle(e)["${args.prop}"]`
				break
			default:
				throw Error(`${NONEXISTENT_EXTRACTION_PROP_ERR}: Non-existant extraction property: ${args.prop}`)
		}

		const selectResult = await select(args.selector)
		const { exceptionDetails, result } = await session.send({
			method: "Runtime.callFunctionOn",
			params: {
				functionDeclaration: `e => ${extractFn}`,
				executionContextId: await session.getExecutionContextId(),
				arguments: [
					{
						objectId: selectResult.objectId,
					},
				],
			},
		})
		if(exceptionDetails) throw Error(`${UNEXPECTED_EXECUTION_RESULT_ERR}: extractFn threw an exception: ${JSON.stringify(exceptionDetails)}`)

		return { [args.key]: result.value }
	}

	let result = undefined
	switch(op) {
		case "goto":
			await _goto()
			break
		case "reload":
			await _reload()
			break
		case "navigateForward":
			await _navigate(1)
			break
		case "navigateBack":
			await _navigate(-1)
			break
		case "mouseMove":
			await _mouseMove()
			break
		case "mouseUp":
			await _mouseButtonEvent("mouseReleased")
			break
		case "mouseDown":
			await _mouseButtonEvent("mousePressed")
			break
		case "scroll":
			await _scroll()
			break
		case "keyDown":
			await _keyEvent("keyDown")
			break
		case "keyUp":
			await _keyEvent("keyUp")
			break
		case "type":
			await _type()
			break
		case "focus":
			await _focus()
			break
		case "extract":
			result = await _extract()
			break
		default:
			throw Error(`${NONEXISTENT_OP_ERR}: ${op}`)
	}

	// Are we on a low-churn event?
	if(op !== "mouseMove" && op !== "scroll") {
		// Wait for a possible navigation
		const didNavigationStartProbe = await Promise.race([
			rpc.notifications.waitForEvent("Page.frameStartedLoading"),
			sleep(100),
		])
		if(didNavigationStartProbe !== undefined) {
			const { frameId } = didNavigationStartProbe.params
			await session.awaitPageLoad(
				rpc.notifications.waitForEvent(
					"Page.frameStoppedLoading",
					msg => msg.params.frameId === frameId),
				frameId)
		}
	}

	return result
}


const close = async session => {
	if(!session.isPageTarget) {
		await session.send({
			method: "Page.close",
			params: {},
		})
	}
}


module.exports = {
	createEngine,
}
