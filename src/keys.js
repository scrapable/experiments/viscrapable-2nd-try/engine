const keyDefinitions = require("./USKeyboardLayout")

/**
 * This function is taken from puppeteer.
 *
 * See ./USKeyboardLayout.js for the license.
 */
const getKeyDescription = (keyString, modifiers) => {
	const shift = modifiers & 8
	const description = {
		key: "",
		keyCode: 0,
		code: "",
		text: "",
		location: 0,
	}

	const definition = keyDefinitions[keyString]
	if(definition === undefined)
		throw Error(`unknownKey: ${keyString}`)

	if (definition.key)
		description.key = definition.key
	if (shift && definition.shiftKey)
		description.key = definition.shiftKey

	if (definition.keyCode)
		description.keyCode = definition.keyCode
	if (shift && definition.shiftKeyCode)
		description.keyCode = definition.shiftKeyCode

	if (definition.code)
		description.code = definition.code

	if (definition.location)
		description.location = definition.location

	if (description.key.length === 1)
		description.text = description.key

	if (definition.text)
		description.text = definition.text
	if (shift && definition.shiftText)
		description.text = definition.shiftText

	// if any modifiers besides shift are pressed, no text should be sent
	if (modifiers & ~8)
		description.text = ""

	return description
}

module.exports = getKeyDescription
