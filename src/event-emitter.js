const createEventEmitter = () => {
	const handlers = {}

	const registerEvent = evtName => {
		handlers[evtName] = []
	}

	const unregisterEvent = evtName => {
		handlers[evtName] = null
	}

	const handlerQueueIsSetUp = evtName =>
		typeof handlers[evtName] === "object"

	const fireEvent = (evtName, evt) => {
		if(!handlerQueueIsSetUp(evtName))
			registerEvent(evtName)

		const asyncHandlerResults = handlers[evtName]
			.filter(handler => typeof handler === "function")
			.map(handler => handler(evt))
			.filter(result => result instanceof Promise)
		return Promise.all(asyncHandlerResults)
	}

	const on = (evtName, handler) => {
		if(!handlerQueueIsSetUp(evtName))
			registerEvent(evtName)

		return handlers[evtName].push(handler)
	}

	const unregister = (evtName, handlerOrID) => {
		const handlerOrIDType = typeof handlerOrID
		const isHandler = handlerOrIDType === "function"
		const isID = handlerOrIDType === "number"

		if(!isHandler && !isID)
			throw Error("invalidHandlerOrID: handlerOrID must be a handler previously registered using 'on' or an ID returned by 'on'")

		if(!handlerQueueIsSetUp(evtName))
			return

		const queue = handlers[evtName]
		const idx = isHandler ? queue.indexOf(handlerOrID) : handlerOrID - 1
		queue[idx] = null
	}

	const waitForEvent = (evtName, predicate = () => true) =>
		new Promise((resolve, reject) => {
			try {
				const id = on(evtName, evt => {
					if(predicate(evt)) {
						unregister(evtName, id)
						resolve(evt)
					}
				})
			} catch(err) {
				reject(err)
			}
		})

	return {
		registerEvent,
		unregisterEvent,
		fireEvent,
		on,
		unregister,
		waitForEvent,
	}
}

module.exports = createEventEmitter
