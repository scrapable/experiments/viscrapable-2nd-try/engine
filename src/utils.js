(async () => {
	const sleep = timeout => new Promise((resolve, _reject) => {
		setTimeout(resolve, timeout)
	})

	/*
		NOTE: This must be a self-contained function without self-
		recursion for puppeteer to be able to execute it in the page
		context.

		It can contain functions as long as there are no outside
		dependencies.
	*/
	const getBestSelector = element => {
		let fullSelector = ""
		let stopUpPropagation = false
		do {
			const parentElement = element.parentElement
			let selector = null
			if(element.id) {
				selector = `#${element.id}`
				stopUpPropagation = true
			}
			else if(element.classList && element.classList.length > 0) {
				selector = `.${[...element.classList].join(".")}`
			} else if(parentElement && element.tagName !== "BODY") {
				const children = parentElement.children
				for(let i = 0; i < children.length; i++) {
					if(children[i] === element) {
						selector = `:nth-child(${i + 1})`
						break
					}
				}
			} else {
				selector = element.tagName
			}
			fullSelector = `${selector} > ${fullSelector}`
			element = parentElement
		// eslint-disable-next-line no-undef
		} while(!stopUpPropagation && element && element !== document.body.parentElement)
		return fullSelector.substr(0, fullSelector.length - 3)
	}

	module.exports = {
		sleep,
		getBestSelector,
	}
})()
