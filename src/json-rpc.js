const createEventEmitter = require("./event-emitter");

(async () => {
	const INVALID_MSG_FORMAT_ERR = "invalidMessageFormat"

	const createRPCConnection = async send => {
		const notificationEmitter = createEventEmitter()
		const responseHandlers = {}
		let currentID = 1

		const recieve = async msg => {
			if(msg.id !== undefined) {
				const handler = responseHandlers[msg.id]
				if(handler) {
					handler(msg)
					delete responseHandlers[msg.id]
				}
			} else {
				if(typeof msg.method !== "string")
					throw Error(`${INVALID_MSG_FORMAT_ERR} method must be a string`)

				await notificationEmitter.fireEvent(msg.method, msg)
			}
		}

		const _send = (msg, expectResponse = true) => new Promise((resolve, reject) => {
			if(typeof msg.method !== "string" || typeof msg.params !== "object")
				reject(Error(`${INVALID_MSG_FORMAT_ERR}: method must be a string and params must be an object`))

			const id = currentID++
			if(expectResponse)
				responseHandlers[id] = resp => {
					if(resp.result)
						resolve(resp.result)
					else if(resp.error)
						reject(resp.error)
					else
						reject(Error(`${INVALID_MSG_FORMAT_ERR}: Response contains neither a result nor an error: ${JSON.stringify(resp)}`))
				}

			send({
				...msg,
				id,
			}, expectResponse)
				.then(() => {
					if(!expectResponse)
						resolve()
				})
				.catch(err => reject(err))
		})

		return {
			send: _send,
			recieve,
			notifications: {
				on: notificationEmitter.on,
				unregister: notificationEmitter.unregister,
				waitForEvent: notificationEmitter.waitForEvent,
			},
		}
	}
	
	module.exports = createRPCConnection
})()
