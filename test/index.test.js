const { default: axios } = require("axios")
const WebSocketConnection = require("ws")
const createRPCConnection = require("../src/json-rpc")
const { createEngine } = require("../src")
const { sleep } = require("../src/utils")

jest.setTimeout(2 * 60 * 1000)  // 2 minutes

const startChromeConnection = async () => {
	const chromePort = 9222
	const chromeHost =
		process.env.SCRAPABLE_TEST_ENV === "gitlab"
			? "chrome"
			: "127.0.0.1"

	console.info("Retrieving debugger URL from Chrome")
	const resp = await axios.get(`http://${chromeHost}:${chromePort}/json/version`, {
		headers: {
			Host: "localhost",
		},
	}).catch(err => {
		if(err.request) {
			console.error("Connection to Chrome failed - Have you started it?")
			console.error(`
To start Chrome on your machine use the scripts in ./dev:
	./dev/start_docker_chrome.sh
		starts a containerized version
		of Chromium that's used for CI
	./dev/start_local_chrome.sh
		starts your local Chromium installation
		(if available) in non-headless mode
			`)
		}
		throw err
	})
	const wsPath = resp.data.webSocketDebuggerUrl.replace(/^ws:\/\/[\w.]+(?::\d+)?\/(.*)/, "$1")

	console.info("Opening WebSocket connection to Chrome")
	const ws = new WebSocketConnection(`ws://${chromeHost}:${chromePort}/${wsPath}`)
	await new Promise(res => {
		ws.on("open", () => {
			res()
		})
	})

	const rpc = await createRPCConnection(msg => new Promise((resolve, reject) => {
		ws.send(JSON.stringify(msg), err => {
			(err ? reject : resolve)(err)
		})
	}))

	ws.on("message", msg => {
		rpc.recieve(JSON.parse(msg))
	})

	const engine = await createEngine(rpc)
	console.info("Connection to Chrome established")

	return {
		ws,
		rpc,
		engine,
	}
}

const closeChromeConnection = async (ws, engine) => {
	await engine.close()
	ws.close()
}

test("goto & navigated", async () => {
	const { ws, engine } = await startChromeConnection()

	const mockHandler = jest.fn()
	engine.on("navigated", mockHandler)

	await engine.execute({
		op: "goto",
		args: {
			url: "https://duck.com",
		},
		delayBefore: 0,
	})

	await closeChromeConnection(ws, engine)

	expect(mockHandler).toBeCalledWith({
		url: "https://duckduckgo.com/",
	})
})

test("close", async () => {
	const { ws, engine } = await startChromeConnection()

	await sleep(500)

	await closeChromeConnection(ws, engine)
})

test("reload", async () => {
	const { ws, engine } = await startChromeConnection()

	const mockHandler = jest.fn()
	engine.on("navigated", mockHandler)

	await engine.execute({
		op: "goto",
		args: {
			url: "https://duck.com",
		},
		delayBefore: 0,
	})

	await engine.execute({
		op: "reload",
		args: {},
		delay: 0,
	})

	await closeChromeConnection(ws, engine)

	expect(mockHandler.mock.calls.length).toBe(2)
})

test("navigate", async () => {
	const { ws, engine } = await startChromeConnection()

	const mockHandler = jest.fn()
	engine.on("navigated", mockHandler)

	await engine.execute({
		op: "goto",
		args: {
			url: "https://duck.com",
		},
		delayBefore: 0,
	})

	expect(mockHandler).lastCalledWith({
		url: "https://duckduckgo.com/",
	})

	await engine.execute({
		op: "goto",
		args: {
			url: "https://news.ycombinator.com",
		},
		delayBefore: 0,
	})

	expect(mockHandler).lastCalledWith({
		url: "https://news.ycombinator.com/",
	})

	await engine.execute({
		op: "navigateBack",
		args: {},
		delayBefore: 0,
	})

	expect(mockHandler).lastCalledWith({
		url: "https://duckduckgo.com/",
	})

	await engine.execute({
		op: "navigateForward",
		args: {},
		delayBefore: 0,
	})

	expect(mockHandler).lastCalledWith({
		url: "https://news.ycombinator.com/",
	})

	await closeChromeConnection(ws, engine)
})

const testDragNDrop = async (networkThrottle = null) => {
	const { ws, engine } = await startChromeConnection()

	const mockNavHandler = jest.fn()
	engine.on("navigated", mockNavHandler)

	if(networkThrottle !== null)
		await engine.cdpSession.send({
			method: "Network.emulateNetworkConditions",
			params: networkThrottle,
		})

	const dragNDrop = require("./drag_n_drop.json")
	for(const evt of dragNDrop) {
		await engine.execute(evt)
	}

	expect(mockNavHandler).toBeCalledWith({
		url: "https://jqueryui.com/droppable/",
	})

	const { result } = await engine.cdpSession.send({
		method: "Runtime.evaluate",
		params: {
			expression: `
				document.querySelector(".demo-frame")
					.contentDocument
					.getElementById("droppable")
					.classList
					.contains("ui-state-highlight")`,
		},
	})
	expect(result.type).toBe("boolean")
	expect(result.value).toBeTruthy()

	await closeChromeConnection(ws, engine)
}

test("drag_n_drop", async () => await testDragNDrop())

test("drag_n_drop on slow network", async () => await testDragNDrop({
	offline: false,
	latency: 100,
	downloadThroughput: 4 * 1024 / 8,
	uploadThroughput: 4 * 1024 / 8,
}))

test("key events", async () => {
	const { ws, engine } = await startChromeConnection()

	await engine.execute({
		op: "goto",
		args: {
			url: "https://duck.com",
		},
		delayBefore: 0,
	})

	const mockNavHandler = jest.fn()
	engine.on("navigated", mockNavHandler)

	await engine.execute({
		op: "type",
		args: {
			text: "Hello World!!!! :)",
		},
		delayBefore: 100,
	})

	await engine.execute({
		op: "keyDown",
		args: {
			code: "Enter",
		},
		delayBefore: 100,
	})

	await engine.execute({
		op: "keyUp",
		args: {
			code: "Enter",
		},
		delayBefore: 50,
	})

	expect(mockNavHandler).toBeCalledWith({
		url: "https://duckduckgo.com/?q=Hello+World%21%21%21%21+%3A%29&t=h_",
	})

	await closeChromeConnection(ws, engine)
})

test("focus", async () => {
	const { ws, engine } = await startChromeConnection()

	await engine.execute({
		op: "goto",
		args: {
			url: "https://jqueryui.com",
		},
		delayBefore: 0,
	})

	const mockNavHandler = jest.fn()
	engine.on("navigated", mockNavHandler)

	const getActiveTag = async () => (await engine.cdpSession.send({
		method: "Runtime.evaluate",
		params: {
			expression: "document.activeElement.tagName",
		},
	})).result.value

	expect(await getActiveTag()).toBe("BODY")

	await engine.execute({
		op: "focus",
		args: {
			selector: "#main > form > label > input[type=text]",
		},
		delayBefore: 100,
	})

	expect(await getActiveTag()).toBe("INPUT")

	await engine.execute({
		op: "type",
		args: {
			text: "test",
		},
		delayBefore: 10,
	})

	await engine.execute({
		op: "keyDown",
		args: {
			code: "Enter",
		},
		delayBefore: 100,
	})

	await engine.execute({
		op: "keyUp",
		args: {
			code: "Enter",
		},
		delayBefore: 50,
	})

	expect(mockNavHandler).toBeCalledWith({
		url: "https://jqueryui.com/?s=test",
	})

	await closeChromeConnection(ws, engine)
})

test("extract", async () => {
	const { ws, engine } = await startChromeConnection()

	await engine.execute({
		op: "goto",
		args: {
			url: "https://jqueryui.com",
		},
		delayBefore: 0,
	})

	const extract = async (selector, prop, key) =>
		await engine.execute({
			op: "extract",
			args: {
				selector,
				prop,
				key,
			},
			delayBefore: 0,
		})

	const extractFromLink = async (prop, key) =>
		await extract(".dev-links a:first-of-type", prop, key)

	const extractFromButton = async (prop, key) =>
		await extract("a[href=\"/resources/download/jquery-ui-1.11.4.zip\"]", prop, key)

	expect(await extractFromLink(
		"href",
		"testExtraction",
	)).toEqual({
		testExtraction: "https://github.com/jquery/jquery-ui",
	})

	expect(await extractFromLink(
		"color",
		"colorOfThing",
	)).toEqual({
		colorOfThing: "rgb(178, 73, 38)",
	})

	expect(await extractFromButton(
		"scrollHeight",
		"height",
	)).toEqual({
		height: 32,
	})

	expect(await extractFromButton(
		"clientHeight",
		"height",
	)).toEqual({
		height: 32,
	})

	expect(await extractFromButton(
		"offsetHeight",
		"height",
	)).toEqual({
		height: 34,
	})

	await closeChromeConnection(ws, engine)
})
