const createRPCConnection = require("../src/json-rpc")

test("just init", async () => {
	await createRPCConnection(() => {})
})

test("send", async () => {
	const mockSend = jest.fn(msg => rpc.recieve({
		id: 1,
		result: {
			x: msg.params.y,
		},
		extra: "something",
	}))

	const rpc = await createRPCConnection(mockSend)

	const resp = await rpc.send({
		method: "Test",
		params: {
			y: 1,
		},
	})

	expect(mockSend).toBeCalled()
	expect(resp).toEqual({
		x: 1,
	})
})

test("on notification", async () => {
	const rpc = await createRPCConnection(() => {})

	const mockNotifHandler = jest.fn()
	rpc.notifications.on("Test", mockNotifHandler)

	rpc.recieve({
		method: "Test",
		params: {},
	})

	expect(mockNotifHandler).toBeCalled()

	rpc.recieve({
		method: "Test2",
		params: {},
	})

	expect(mockNotifHandler.mock.calls.length).toBe(1)
})

test("unregister notification handler", async () => {
	const rpc = await createRPCConnection(() => {})

	const mockNotifHandler = jest.fn()
	rpc.notifications.on("Test", mockNotifHandler)

	rpc.notifications.unregister("Test", () => {})

	rpc.recieve({
		method: "Test",
		params: {},
	})

	expect(mockNotifHandler).toBeCalled()

	rpc.notifications.unregister("Test", mockNotifHandler)

	rpc.recieve({
		method: "Test",
		params: {},
	})

	expect(mockNotifHandler.mock.calls.length).toBe(1)
})

test("waitForNotification", async () => {
	const rpc = await createRPCConnection(() => {})

	let notifHasBeenRecieved = false

	setTimeout(() => {
		notifHasBeenRecieved = true
		rpc.recieve({
			method: "Test",
			params: {},
		})
	}, 250)

	expect(notifHasBeenRecieved).not.toBeTruthy()
	await rpc.notifications.waitForEvent("Test")
	expect(notifHasBeenRecieved).toBeTruthy()
})
